use ggez::event::{self, EventHandler, KeyCode, KeyMods};
use ggez::graphics::{self, DrawMode, DrawParam, Drawable, MeshBuilder};
use ggez::{nalgebra as na, timer, Context, ContextBuilder, GameResult};
use std::collections::VecDeque;

fn main() {
    let (mut ctx, mut event_loop) = ContextBuilder::new("game_name", "author_name")
        .build()
        .unwrap();
    let mut my_game = MyGame::new(&mut ctx);

    // Run!
    match event::run(&mut ctx, &mut event_loop, &mut my_game) {
        Ok(_) => println!("Exited cleanly."),
        Err(e) => println!("Error occured: {}", e),
    }
}
struct Epicycle {
    radius: f32,
    color: graphics::Color,
    x: f32,
    y: f32,
    angular_speed: f32,
    angle: f32,
}
impl Drawable for Epicycle {
    fn draw(&self, ctx: &mut Context, param: DrawParam) -> GameResult {
        let mesh = MeshBuilder::new()
            .circle(
                DrawMode::stroke(1.0),
                [(self.x), (self.y)],
                self.radius,
                0.1,
                graphics::Color::new(0.0, 0.0, 0.0, 0.5),
            )
            .line(
                &[
                    [(self.x), (self.y)],
                    [(self.x + self.cos()), (self.y + self.sin())],
                ][..],
                1.0,
                self.color,
            )?
            .build(ctx)?;

        graphics::draw(ctx, &mesh, param)?;
        Ok(())
    }
    fn dimensions(&self, _ctx: &mut Context) -> Option<graphics::Rect> {
        Some(graphics::Rect::new(
            self.x,
            self.y,
            self.radius * 2.0,
            self.radius * 2.0,
        ))
    }
    fn set_blend_mode(&mut self, _mode: Option<graphics::BlendMode>) {}
    fn blend_mode(&self) -> Option<graphics::BlendMode> {
        Some(graphics::BlendMode::Alpha)
    }
}
impl Epicycle {
    fn new() -> Epicycle {
        Epicycle {
            radius: 100.0,
            color: graphics::BLACK,
            x: 75.0,
            y: 512.0 / 2.0,
            angular_speed: 1.0 / 5.0,
            angle: 0.0,
        }
    }
    fn cos(&self) -> f32 {
        self.radius * (self.angle).cos()
    }
    fn sin(&self) -> f32 {
        self.radius * (self.angle).sin()
    }
    fn get_absolute_pos(&self) -> (f32, f32) {
        (self.get_absolute_x(), self.get_absolute_y())
    }
    fn get_absolute_x(&self) -> f32 {
        self.x + self.cos()
    }
    fn get_absolute_y(&self) -> f32 {
        self.y + self.sin()
    }
    fn update(&mut self, dt: f32, pos: (f32, f32)) {
        self.angle += (2.0 * std::f32::consts::PI * self.angular_speed * dt) / (1000.0) as f32;
        self.angle %= 2.0 * std::f32::consts::PI;
        self.x = pos.0;
        self.y = pos.1;
    }
}

struct Graph {
    points: VecDeque<na::Point2<f32>>,
}
impl Drawable for Graph {
    fn draw(&self, ctx: &mut Context, param: DrawParam) -> GameResult {
        let mb = MeshBuilder::new()
            .line(&self.points.as_slices().0, 1.0, [0.0, 0.0, 0.0, 1.0].into())?
            .build(ctx)?;
        graphics::draw(ctx, &mb, param)?;
        Ok(())
    }
    fn dimensions(&self, _ctx: &mut Context) -> Option<graphics::Rect> {
        todo!()
    }
    fn set_blend_mode(&mut self, _mode: Option<graphics::BlendMode>) {
        todo!()
    }
    fn blend_mode(&self) -> Option<graphics::BlendMode> {
        Some(graphics::BlendMode::Alpha)
    }
}
impl Graph {
    fn update(&mut self, pos: &mut na::Point2<f32>, dt: f32) {
        pos.as_mut().x = pos.as_mut().x + 200.0;
        self.points.push_front(*pos);
        self.points.iter_mut().for_each(|p| {
            p.x += dt / 20.0 as f32;
        });
    }
}
struct MyGame {
    epicycles: Vec<Epicycle>,
    graph: Graph,
    n_epi: usize,
}

impl MyGame {
    pub fn new(_ctx: &mut Context) -> MyGame {
        let mut g = MyGame {
            n_epi: 7,
            epicycles: Vec::with_capacity(7),
            graph: Graph {
                points: VecDeque::with_capacity(3000),
            },
        };
        g.epicycles.push(Epicycle::new());

        for k in 1..g.n_epi {
            g.epicycles.push(Epicycle {
                radius: 4.0 / (std::f64::consts::PI as f32 * (2 * k + 1) as f32) * 100.0,
                color: [0.0, 0.0, 0.0, 1.0].into(),
                x: g.epicycles[k - 1].get_absolute_x(),
                y: g.epicycles[k - 1].get_absolute_y(),
                angular_speed: (2 * k + 1) as f32 / 5.0 as f32,
                angle: 0.0,
            });
        }
        g
    }
}

impl EventHandler for MyGame {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        self.epicycles[0].update(timer::delta(ctx).as_millis() as f32, (150.0, 512.0 / 2.0));
        let mut last_pos = self.epicycles[0].get_absolute_pos();
        self.epicycles.iter_mut().skip(1).for_each(|epi| {
            epi.update(timer::delta(ctx).as_millis() as f32, last_pos);
            last_pos = epi.get_absolute_pos();
        });
        let (_, y) = last_pos;

        self.graph.update(
            &mut na::Point2::new(150.0 + 100.0, y),
            timer::delta(ctx).as_millis() as f32,
        );
        let dif: i32 = self.n_epi as i32 - self.epicycles.len() as i32;
        // if dif > 0 {
        //     let offset = self.epicycles.len();
        //     for k in 0..dif {
        //         self.epicycles.push(Epicycle {
        //             radius: 4.0
        //                 / (std::f64::consts::PI as f32 * (2 * (k + offset as i32) + 1) as f32)
        //                 * 100.0,
        //             color: [0.0, 0.0, 0.0, 1.0].into(),
        //             x: 0.0,
        //             y: 0.0,
        //             angular_speed: (2 * (k + offset as i32) + 1) as f32 / 5.0 as f32,
        //             angle: 0.0,
        //         });
        //     }
        // } 
        if dif < 0 {
            let dif = dif.abs();
            for _ in 0..dif {
                self.epicycles.pop();
            }
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, graphics::WHITE);

        self.epicycles.iter().for_each(|epi| {
            epi.draw(ctx, DrawParam::default()).unwrap();
        });
        match self.graph.draw(ctx, DrawParam::default()) {
            Err(e) => {
                dbg!(e);
            }
            Ok(()) => {}
        }
        let (x, y) = self.epicycles[self.epicycles.len() - 1].get_absolute_pos();
        let xe = self.graph.points[0].as_mut().x;
        let mb = MeshBuilder::new()
            .line(
                &[na::Point2::new(x, y), na::Point2::new(xe, y)][..],
                1.0,
                [0.0, 0.0, 1.0, 1.0].into(),
            )?
            .build(ctx)?;
        graphics::draw(ctx, &mb, DrawParam::default())?;
        graphics::present(ctx)
    }
    fn key_up_event(&mut self, _ctx: &mut Context, keycode: KeyCode, keymod: KeyMods) {
        match keycode {
            KeyCode::M => {
                self.n_epi += 1;
            }
            KeyCode::L => {
                if self.n_epi != 0 {
                    self.n_epi -= 1;
                }
            }
            _ => {
                println!("Key released: {:?}, modifier {:?}", keycode, keymod);
            }
        }
    }
}
