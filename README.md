
This is a simple programm : it calculates 7 termes of the fourrier series for the square wave, 

  - you can supress termes with the `l` key   
  
if you want a better approximation, juste increase the value of `n_epi` on line 135 and recompile

You can learn more about the square wave [here](https://en.wikipedia.org/wiki/Square_wave
)![Unable to show the pic](screenshot.png "Title")